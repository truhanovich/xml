﻿<?xml version='1.0'?>
<xsl:stylesheet version="1.0"
      xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
      xmlns:bk="http://library.by/catalog"
  extension-element-prefixes="bk">

  <xsl:template match="/bk:catalog">
    <html>
      <body>
        <table border="1" cellpadding="2" cellspacing="0" width="50%">
          <xsl:apply-templates/>
        </table>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="bk:book">
    <tr>
      <xsl:value-of select="cd:title"/>
    </tr>
  </xsl:template>
  
  <xsl:template match="text() | @*"/>
</xsl:stylesheet>