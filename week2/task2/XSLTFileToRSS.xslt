﻿<?xml version='1.0'?>
<xsl:stylesheet version="1.0"
      xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
      xmlns:bk="http://library.by/catalog"
  extension-element-prefixes="bk">

  <xsl:output method="xml"/>

  <xsl:template match="/bk:catalog">
    <rss version="2.0">
      <channel>
        <title>Book</title>
        <link>http://liftoff.msfc.nasa.gov/</link>
        <description>book</description>
        <language>en-us</language>
        <pubDate>Tue, 10 Sep 2016 04:00:00 GMT</pubDate>
        <generator>Weblog Editor 2.0</generator>
        <managingEditor>test@test.com</managingEditor>
        <webMaster>test@test.com</webMaster>

        <xsl:apply-templates/>
      </channel>
    </rss>
  </xsl:template>

  <xsl:template match="bk:book">
    <xsl:element name="item">
      <title>
        <xsl:value-of select="bk:title"/>
      </title>
      <xsl:if test="bk:isbn/node()">
        <link>
          <xsl:value-of select="concat('http://my.safaribooksonline.com/', bk:isbn)"/>/
        </link>
      </xsl:if>
      <description>
        <xsl:value-of select="bk:description"/>
      </description>
      <pubDate>
        <xsl:value-of select="bk:publish_date"/>
      </pubDate>
      <guid>
        <xsl:value-of select="./@id"/>
      </guid>
      <xsl:apply-templates/>
    </xsl:element>
  </xsl:template>


  <xsl:template match="text() | @*"/>
</xsl:stylesheet>
