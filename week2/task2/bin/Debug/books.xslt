﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xml:space="default">
  
  <xsl:output method="xml" indent="yes"/>

  <xsl:template match="namelist/name">
    <xsl:apply-templates/>
    <xsl:if test="position()!=last()">, </xsl:if>
  </xsl:template>

</xsl:stylesheet>
