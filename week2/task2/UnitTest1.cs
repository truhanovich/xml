﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Xml.Xsl;

namespace task2
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void RssFunction()
        {
            var xsl = new XslCompiledTransform();
            var settings = new XsltSettings { EnableScript = true };
            xsl.Load("XSLTFileToRSS.xslt", settings, null);

            xsl.Transform("books.xml", null, Console.Out);
        }
    }
}
