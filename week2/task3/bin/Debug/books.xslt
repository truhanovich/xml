﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xml:space="default">

  <xsl:output method="xml" indent="yes"/>

  <xsl:template match="/">
    <rss version="2.0">
      <channel>
        <title>Book</title>
        <link>http://liftoff.msfc.nasa.gov/</link>
        <description>book</description>
        <language>en-us</language>
        <pubDate>Tue, 10 Sep 2016 04:00:00 GMT</pubDate>

        <generator>Weblog Editor 2.0</generator>
        <managingEditor>test@test.com</managingEditor>
        <webMaster>test@test.com</webMaster>

        <xsl:apply-templates/>
      </channel>
    </rss>
  </xsl:template>

  <xsl:template match="book">
    <xsl:element name="item">
      <title>
        <xsl:value-of select="title"/>
      </title>
      <xsl:if test="/./isbn/node()">
        <link>
          <xsl:value-of select="concat('http://my.safaribooksonline.com/', . /title/text)"/>/
        </link>
      </xsl:if>
      <description>
        <xsl:value-of select="description"/>
      </description>
      <pubDate>
        <xsl:value-of select="publish_date"/>
      </pubDate>
      <guid>
        <xsl:value-of select="publish_date"/>
      </guid>
      <xsl:apply-templates/>
    </xsl:element>
  </xsl:template>

</xsl:stylesheet>
