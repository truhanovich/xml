﻿<?xml version='1.0'?>
<xsl:stylesheet version="1.0"
      xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
      xmlns:msxsl="urn:schemas-microsoft-com:xslt"
      xmlns:bk="http://library.by/catalog"
      xmlns:user="urn:my-scripts"
      xmlns:ext="http://epam.com/xsl/ext"
      extension-element-prefixes="bk">


  <xsl:output method="html"/>

  <xsl:template match="/bk:catalog">
    <html>

      <body>
        <h1>
          Текущие фонды по жанрам
          <xsl:value-of select="ext:Date(yyyy-MMMM-dd)"/>
        </h1>

        <table border="1" cellpadding="2" cellspacing="0" width="50%">
          <xsl:apply-templates/>
        </table>
      </body>
      <h1>
        <xsl:value-of select="ext:GetResult()"/>
      </h1>
    </html>
  </xsl:template>

  <xsl:template match="bk:book">
    <tr>
      <tr>
        <xsl:value-of select="bk:author"/>
      </tr>
      <tr>
        <xsl:value-of select="bk:title"/>
      </tr>
      <tr>
        <xsl:value-of select="bk:publish_date"/>
      </tr>
      <tr>
        <xsl:value-of select="bk:registration_date"/>
      </tr>
    </tr>
    <xsl:value-of select="ext:GetAndInc(bk:genre)"/>
  </xsl:template>

  <xsl:template match="text() | @*"/>
</xsl:stylesheet>