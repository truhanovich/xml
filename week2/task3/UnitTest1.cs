﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Xml.Xsl;
using System.IO;

namespace task3
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            var args = new XsltArgumentList();
            args.AddExtensionObject("http://epam.com/xsl/ext", new Counters());

            var xsl = new XslCompiledTransform();
            //var settings = new XsltSettings { EnableScript = true };
            xsl.Load("XSLTFile1.xslt");

            xsl.Transform("books.xml", args, Console.Out);

            xsl.Transform("books.xml", args, new FileStream("1.html", FileMode.OpenOrCreate));
        }
    }
}
