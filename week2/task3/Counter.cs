﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace task3
{
    public class Counters
    {
        private Dictionary<string, int> counters = new Dictionary<string, int>();

        public int GetAndInc(string name)
        {
            int result = 0;
            counters.TryGetValue(name, out result);
            counters[name] = result + 1;
            return result;
        }

        public string GetResult()
        {
            return string.Join(";", counters.Select(s => $"{s.Key} : {s.Value}").ToArray());
        }

        public string Date(string str)
        {
            return DateTime.Now.ToString(str);
        }
    }
}
